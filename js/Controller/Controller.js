export class Model {
  constructor(_id, _contentTask, _statusTask) {
    this.id = _id;
    this.contentTask = _contentTask;
    this.statusTask = _statusTask;
  }
}

let id = 0;

export let getInforTask = () => {
  id++;
  let content = document.getElementById("newTask").value;
  let status = false;
  if (content == "") return;
  return new Model(id, content, status);
};

export const renderList = (taskArray, nameHTML) => {
  let contentHTML = "";
  taskArray.forEach((item) => {
    let contentLi = `
        <li>
            <span>${item.contentTask}</span>
            <div class="buttons">
                <button class="remove">
                    <i class="far fa-trash-alt" onclick="removeTask(${item.id}, ${item.statusTask})"></i>
                </button>
                <button class="complete">
                    <i class="fas fa-check-circle" onclick="moveFormComplete(${item.id})"></i>
                    <i class="far fa-check-circle" onclick="moveToComplete(${item.id})"></i>
                </button>
            </div>
        </li>
    `;
    contentHTML += contentLi;
  });
  document.getElementById(nameHTML).innerHTML = contentHTML;
};

export let findIndexTask = (id, listTask) => {
  return listTask.findIndex((item) => {
    return item.id == id;
  });
};

export function resetForm() {
  document.getElementById("newTask").value = "";
}
