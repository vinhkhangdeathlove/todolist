import {
  getInforTask,
  renderList,
  resetForm,
  findIndexTask,
} from "./Controller/Controller.js";

let listTask = [];
let listTaskComplete = [];

let addNewTask = () => {
  let newTask = getInforTask();
  if (newTask != undefined) {
    listTask.push(newTask);
    resetForm();
  }
  renderList(listTask, "todo");
};

let changeTask = (index, list1, list2) => {
  list2.push(list1[index]);
  list1.splice(index, 1);
  renderListTask();
  resetForm();
};

let moveToComplete = (id) => {
  let index = findIndexTask(id, listTask);
  listTask[index].statusTask = true;
  changeTask(index, listTask, listTaskComplete);
};

let moveFormComplete = (id) => {
  let index = findIndexTask(id, listTaskComplete);
  listTaskComplete[index].statusTask = false;
  changeTask(index, listTaskComplete, listTask);
};

let removeTask = (id, statusTask) => {
  if (statusTask) {
    let index = findIndexTask(id, listTaskComplete);
    listTaskComplete.splice(index, 1);
  } else {
    let index = findIndexTask(id, listTask);
    listTask.splice(index, 1);
  }
  renderListTask();
};

let renderListTask = () => {
  renderList(listTask, "todo");
  renderList(listTaskComplete, "completed");
};

document.getElementById("two").addEventListener("click", () => {
  listTask.sort((a, b) => {
    let fa = a.contentTask;
    let fb = b.contentTask;
    if (fa < fb) return -1;
    if (fa > fb) return 1;
    return 0;
  });
  renderList(listTask, "todo");
});

document.getElementById("three").addEventListener("click", () => {
  listTask.sort((a, b) => {
    let fa = a.contentTask;
    let fb = b.contentTask;
    if (fa > fb) return -1;
    if (fa < fb) return 1;
    return 0;
  });
  renderList(listTask, "todo");
});

window.addNewTask = addNewTask;
window.moveToComplete = moveToComplete;
window.moveFormComplete = moveFormComplete;
window.removeTask = removeTask;
